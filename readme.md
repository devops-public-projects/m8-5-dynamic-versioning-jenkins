
# Dynamically Increment Versions in Jenkins Pipeline
In this project we will configure dynamic increments for version numbers in our Jenkins CI Pipeline

## Technologies Used
- Jenkins
- Docker
- GitLab
- Git
- Java
- Maven

## Project Description
- Configure CI Step: Increment patch number
- Configure CI Step: Build Java application and clean old artifacts
- Configure CI Step: Build Image with dynamic Docker Image Tag
- Configure CI Step: Push image to private DockerHub Repository
- Configure CI Step: Commit version update of Jenkins back to the Git repository
- Configure Jenkins Pipeline to not trigger automatically on CI build commit to avoid commit loop

## Prerequisites
- Jenkins installed on a remote server as a Docker container
- Docker Hub account
- Gitlab account and a repository
- Private Repository with the **java-maven-app** code in it. [Gitlab Link](https://gitlab.com/twn-devops-bootcamp/latest/08-jenkins/java-maven-app)

## Guide Steps
### Increment App Version
- In our **Jenkinsfile** we will add a new stage for "Increment Version"
- It will include a script from the Maven plugin **build-helper**
```groovy
script {
	echo 'Incrementing App Version'
	sh `mvn build-helper:parse-version versions:set \
	-DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
	versions:commit'
}
```
### Increment Docker Image Version
We will set the Docker Versions the same as the JAR versions
- Next, we will modify the **Build Image** stage in our **Jenkinsfile** to have our docker commands use a variable instead of a hard coded value.
```groovy
sh "docker build -t DOCKER_HUB_UNAME/demo-app:${IMAGE_NAME} ."
sh 'echo $PASS | docker login -u $USER --password-stdin'
sh "docker push DOCKER_HUB_UNAME/demo-app:${IMAGE_NAME}"
```
- We now need to get the **IMAGE_NAME** from the JAR version which will be obtained from the **Increment Version** stage. After the **build-helper** command we will add these extra steps.
```groovy
def matcher = readFile('pom.xml') =~ '<version>(.*)</version>'
def version = matcher[0][1]
env.IMAGE_NAME = "$version-$BUILD_NUMBER"
```
### Modify Dockerfile
- We will remove any hardcoding versions/file names and create a dynamic workflow. We will need to modify the line for **COPY** and change **ENTRYPOINT** to **CMD**
```dockerfile
COPY ./target/java-maven-app-*.jar /usr/app/
CMD java -jar java-maven-app-*.jar
```

### Clean Artifacts Before Building
- We will modify the **Build App** stage and add `clean` to our `mvn package command` to first delete the previous build before we build the new one.
```groovy
echo 'Building application'
sh 'mvn clean package'
```

Since we previously configured automatic builds based on Webhooks the build automatically ran when the new branch was created and then again when the final changes were added.

![Build Completed Successfully](/images/m8-5-successful-automatic-builds.png)
![Successful Docker Name and Upload](/images/m8-5-incremented-version-docker-hub.png)

### Prevent Commit Loop
- In our Jenkins file we will add a new stage for **commit version update**
	- Set the origin to the HEAD of the branch you are using
	- Note: Your USER variable needs to be your GitLab username not email. Using an email will parse incorrectly and cause an error.
```groovy
withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
	sh 'git config --global user.email "jenkins@example.com"'
	sh 'git config --global user.name "jenkins"'

	sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/PATH_TO_PROJECT.GIT"
	sh 'git add .'
	sh 'git commit -m "Jenkins: version bump"'
	sh 'git push origin HEAD:incremental-versioning'
}
```
![Jenkins Updated Pom XML in GitLab](/images/m8-5-successful-jenkins-version-update-gitlab.png)

### Ignore Jenkins Commit in CI Trigger
We will utilize a plugin **Ignore Committer Strategy**
- Manage Jenkins > Plugins > Available Plugins > Ignore Committer Strategy > **Install without restart**
- Go to your Multibranch Pipeline > **Configure**
- Build Sources > Build strategies > **Ignore Committer Strategy**
	- Author emails to ignore:  jenkins@example.com
	- Check off **Allow builds when a changeset contains non-ignored author(s)**
- **Save**

![Ignore Committer Strategy Email](/images/m8-5-ignore-committer-strategy.png)

![Successful Committer Ignore Strategy Build](/images/m8-5-successful-committer-ignore.png)